//offline mode
idb.enablePersistence()
  .catch(function(err) {
    if (err.code == 'failed-precondition') {
      // probably multible tabs open at once
      console.log('persistance failed');
    } else if (err.code == 'unimplemented') {
      // lack of browser support for the feature
      console.log('persistance not available');
    }
  });

//render data
idb.collection('recipes').onSnapshot((snapshot) =>{
    console.log(snapshot);
    snapshot.docChanges().forEach(change => {
        if(change.type === "added"){
          renderRecipe(change.doc.data(), change.doc.id);
        }
        if(change.type ==="removed"){
          removeRecipe(change.doc.id);
        }
    })
})

//add
const form = document.querySelector('form');
form.addEventListener('submit', etv =>{
  etv.preventDefault();

  const newRecipe = {
    title: form.title.value,
    ingredients: form.ingredients.value,
    status: form.status.checked
  }

  idb.collection('recipes').add(newRecipe)
     .catch(err => console.log(err));
     form.title.value = '';
     form.ingredients.value = '';
     form.status.checked = '';
})

const recipeContainer = document.querySelector('.recipes');

//delete
recipeContainer.addEventListener('click', evt => { 
  console.log(evt);
  const id = evt.target.getAttribute('data-id');
  console.log(id);
  if(evt.target.tagName === 'I') {
    console.log('delete');
    // const id = evt.target.getAttribute('data-id');
    idb.collection('recipes').doc(id).delete();
  } else if (evt.target.tagName === 'IMG') {
    window.location.href = `/pages/detail.html?id=${id}`;
  }
})